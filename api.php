<?php
 include "config.php";
// get the HTTP method, path and body of the request.
$method = $_SERVER['REQUEST_METHOD'];
$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
$input = json_decode(file_get_contents('php://input'),true);
if (empty($input)){
    $input = [];
}
 
// connect to the mysql database
mysqli_set_charset($link,'utf8');
 
// retrieve the table and key from the path
$table = preg_replace('/[^a-z0-9_]+/i','',array_shift($request));
$key = array_shift($request);
if ($key == "login" && isset($input)){
  $email = $input['email'];
  $senha = $input['senha'];
}
 
// escape the columns and values from the input object
$columns = preg_replace('/[^a-z0-9_]+/i','',array_keys($input));
$values = array_map(function ($value) use ($link) {
  if ($value===null) return null;
  return mysqli_real_escape_string($link,(string)$value);
},array_values($input));
 
// build the SET part of the SQL command
$set = '';
for ($i=0;$i<count($columns);$i++) {
  $set.=($i>0?',':'').'`'.$columns[$i].'`=';
  $set.=($values[$i]===null?'NULL':'"'.$values[$i].'"');
}
// create SQL based on HTTP method
switch ($method) {
  case 'GET':
    $sql = "select * from `$table`".($key?" WHERE id=$key":''); 
    break;
  case 'PUT':
    $sql = "update `$table` set $set where id=$key"; break;
  case 'POST':
    $sql = "insert into `$table` set $set";
    if ($key == 'login')
    $sql = "select id, name, email, username, active from `user` WHERE email='".$email."' AND password='".$senha."'"; 
    break;
  case 'DELETE':
    $sql = "delete `$table` where id=$key"; break;
}

// excecute SQL statement
$result = mysqli_query($link,$sql);
// die if SQL statement failed
if (!$result) {
  http_response_code(404);
  die(mysqli_error($link));
}
 
// print results, insert id or affected row count
if ($method == 'GET' || $key == 'login') {
//  if (!$key) echo '[';
//     function get_web_page($url) {
//     $options = array(
//         CURLOPT_RETURNTRANSFER => true,   // return web page
//         CURLOPT_HEADER         => false,  // don't return headers
//         CURLOPT_FOLLOWLOCATION => true,   // follow redirects
//         CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
//         CURLOPT_ENCODING       => "",     // handle compressed
//         CURLOPT_USERAGENT      => "test", // name of client
//         CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
//         CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
//         CURLOPT_TIMEOUT        => 120,    // time-out on response
//     ); 

//     $ch = curl_init($url);
//     curl_setopt_array($ch, $options);

//     $content  = curl_exec($ch);

//     curl_close($ch);
//     return $content;
// }
  for ($i=0;$i<mysqli_num_rows($result);$i++) {
    echo json_encode(mysqli_fetch_object($result));
  }
//  if (!$key) echo ']';
} elseif ($method == 'POST') {
    echo mysqli_insert_id($link);
} else {
  echo mysqli_affected_rows($link);
}
 
// close mysql connection
mysqli_close($link);